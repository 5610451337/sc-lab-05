package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Model.Book;
import View.GUI;

public class ConLibrary {
	private GUI frame;
	private Book book;
	
	public static void main(String[] args){
		new ConLibrary();
	}
	
	public ConLibrary(){
		frame = new GUI();
		book = new Book();
		frame.setVisible(true);
		frame.setSize(350,300);
		frame.setLocation(200,150);
		frame.setButtonName(new AddButtonName());
		frame.setButtonBook1(new AddButtonBook1());
		frame.setButtonBook2(new AddButtonBook2());
		Testcase();
	}
	class AddButtonName implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			String idname = frame.getID();
			
			if(frame.getCombo()=="Student"){
				frame.setText("Student " + idname);
			}
			else if(frame.getCombo()=="Teacher"){
				frame.setText("Teacher "+idname);
			}
			else if(frame.getCombo()=="Personnel"){
				frame.setText("Personnel "+idname);
			}
		}
		
	}
	
	class AddButtonBook1 implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			String b = frame.getBook1();
			frame.setBook1(book.setBook(b));
			frame.setText(book.setBook(b));
		}
	}
	
	class AddButtonBook2 implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			String b = frame.getBook2();
			
		}
		
	}
	
	public void Testcase(){
		frame.setText("Student "+"5610451191");
		frame.setText(book.setBook("A001"));
		frame.setText(book.setBook("D099"));
	}
		
	
}
