package Model;

import java.util.ArrayList;


public class Library {
	private ArrayList<Book> Book_List = new ArrayList<Book>();
	private ArrayList<User> User_List = new ArrayList<User>();
	
	public void addBook(String id){
		Book_List.add(new Book(id));
	}
	public void addStudent(String id){
		User_List.add(new Student(id));
	}
	public void addTeacher(String id){
		User_List.add(new Teacher(id));
	} 
	public void addPersonnel(String id){
		User_List.add(new Personnel(id));
	}

}
