package Model;

import java.util.ArrayList;


public class Book {
	private String Book_id;
	private String Book_name;
	private boolean Book_status;
	private ArrayList<String> listbook;
	private ArrayList<String> namebook;
	
	
	public Book(){
		addBook();
		Book_status = false;
	}
	public Book(String id){
		addBook();
		Book_id = id;
		Book_status = false;
	}
	
	public void addBook(){
		listbook = new ArrayList<String>();
		namebook = new ArrayList<String>();
		listbook.add("A001");
		namebook.add("Big Java");
		listbook.add("B077");
		namebook.add("Database");
		listbook.add("C077");
		namebook.add("Calulus");
		listbook.add("A009");
		namebook.add("Static");
		listbook.add("D099");
		namebook.add("Car");
		listbook.add("E010");
		namebook.add("Tree");
		listbook.add("Z002");
		namebook.add("Ant");
		listbook.add("M081");
		namebook.add("Cat");
		listbook.add("M004");
		namebook.add("Dog");
		listbook.add("G043");
		namebook.add("Egg");
	}
	
	
	public String setBook(String idbook){
		for(int i=0;i<listbook.size();i++){
			if(listbook.get(i).equals(idbook)){
				Book_name = namebook.get(i);
			}
		}
		return Book_name;
	}
	
	public String getName(){
		return Book_name;
	}
	public String getIdBook(){
		return Book_id;
	}
	public boolean checkStatusBook(){
		return Book_status;
	}
	public void changStatusBook(){
		if(Book_status == true){
			Book_status = false;
			}
		else {
			Book_status = true;
		}
	}
}
