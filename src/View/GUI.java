package View;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GUI extends JFrame{
	private JTextArea resultsBook;
	private JTextArea resultsBorrow;
	private JTextArea resultsReturn;
	private JComboBox<Object> comboBox;
	private JPanel userPanel;
	private JPanel listbookPanel;
	private JPanel BookPanel;
	private JPanel book1;
	private JPanel book2;
	private JButton SubmitName;
	private JButton SubmitBook1;
	private JButton SubmitBook2;
	private JTextField IDUser;
	private JTextField IDBorrow;
	private JTextField IDReturn;
	private JLabel ID_user;
	private JLabel Borrow;
	private JLabel Return;
	private JScrollPane scroll;
	private String str = "";
	
	public GUI(){
		createFrame();
		createPanel();
	}
	
	public void createFrame(){
		comboBox = new JComboBox<>();
		comboBox.addItem("Student");
		comboBox.addItem("Teacher");
		comboBox.addItem("Personnel");
		ID_user = new JLabel("Enter User ID");
		IDUser = new JTextField(20);
		SubmitName = new JButton("Submit");
		
		resultsBook = new JTextArea("");
		scroll = new JScrollPane(resultsBook);
		
		Borrow = new JLabel("Borrow");
		IDBorrow = new JTextField(10);
		SubmitBook1 = new JButton("Submit");
		resultsBorrow = new JTextArea(""+str);
		
		
		Return = new JLabel("Return");
		IDReturn = new JTextField(10);
		SubmitBook2 = new JButton("Submit");
		resultsReturn = new JTextArea(""+str);
		
		
	}
	public void createPanel(){
		userPanel = new JPanel();
		userPanel.setLayout(new GridLayout(3,1));
		BookPanel = new JPanel();
		BookPanel.add(comboBox);
		BookPanel.add(ID_user);
		BookPanel.add(IDUser);
		BookPanel.add(SubmitName);
		userPanel.add(BookPanel);
		userPanel.add(scroll);
		listbookPanel = new JPanel();
		listbookPanel.setLayout(new GridLayout(1,2));
		book1 = new JPanel();
		book1.add(Borrow);
		book1.add(IDBorrow);
		book1.add(resultsBorrow);
		book1.add(SubmitBook1);
		book2 = new JPanel();
		book2.add(Return);
		book2.add(IDReturn);
		book2.add(resultsReturn);
		book2.add(SubmitBook2);
		listbookPanel.add(book1);
		listbookPanel.add(book2);
		userPanel.add(listbookPanel);
		
		
		add(userPanel,BorderLayout.CENTER);
		
	}
	
	public String setNamebook(String bookname){
		str = bookname;
		return str;
	}
	
	public String getCombo(){
		return (String) comboBox.getSelectedItem();
	}
	public String getID(){
		return IDUser.getText();
	}
	public String getBook1(){
		return IDBorrow.getText();
	}
	public String getBook2(){
		return IDReturn.getText();
	}
	
	
	public void setButtonName(ActionListener list){
		SubmitName.addActionListener(list);
	}
	public void setButtonBook1(ActionListener list){
		SubmitBook1.addActionListener(list);

	}
	public void setButtonBook2(ActionListener list){
		SubmitBook2.addActionListener(list);
	}
	
	public void setText(String s){
		String str = resultsBook.getText();
		resultsBook.setText(str+"\n"+s);
	}
	
	public void setBook1(String str){
		this.str = str;
		resultsBorrow.setText(this.str);
	}

	
}
